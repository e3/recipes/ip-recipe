# ip conda recipe

Home: https://github.com/epics-modules/ip

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS ip module
